using System.Collections.Generic;
using Domain.FuzzyStringLib.PropertyMatchers;

namespace Domain.FuzzyStringLib
{
     /// <summary>
    /// This class is responsible to create proper strategy according to the agency
    /// </summary>
    public class PropertyMatchingRuleFactory
    {
        private static Dictionary<string, IPropertyMatcher> _strategyCache = new Dictionary<string, IPropertyMatcher>();

        public static IPropertyMatcher Create(string agentCode)
        {
            switch (agentCode)
            {
                case "OTBRE":
                    return GetOrAdd<OTBREPropertyMatcher>(agentCode);
                case "LRE":
                    return GetOrAdd<LREPropertyMatcher>(agentCode);
                case "CRE":
                    return GetOrAdd<CREPropertyMatcher>(agentCode);
                default:
                    return GetOrAdd<GenericStrategy>(agentCode);
            }
        }

        private static T GetOrAdd<T>(string key)
            where T : IPropertyMatcher, new()
        {
            T result;
            if (_strategyCache.ContainsKey(key))
            {
                result = (T)_strategyCache[key];
            }
            else
            {
                result = new T();
                _strategyCache[key] = result;
            }
            return result;
        }
    }
}