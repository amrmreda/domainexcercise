﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Domain.FuzzyStringLib.Helpers
{
    public class StringHelper
    {
        private static string _regularExpressionWithPunctuationMarks = @"[^A-Za-z0-9]";
         
        /// <summary>
        /// Compare two strings (WithPunctuation is optional)  
        /// </summary>
        /// <param name="stringA"></param>
        /// <param name="stringB"></param>
        /// <param name="withPunctuation"></param>
        /// <returns></returns>
        public static bool CompareStrings(string stringA, string stringB, bool withPunctuation = false)
        {
            if (withPunctuation)
            {
                stringA = RemovePunctuation(stringA);
                stringB = RemovePunctuation(stringB);
            }
             
            return string.Compare(stringA, stringB, StringComparison.OrdinalIgnoreCase) == 0;
        }
         
        /// <summary>
        /// Receive string of words and return them in the reversed order.
        /// </summary>
        public static string ReverseWords(string sentence)
        {
            string[] words = sentence.Split(' ');
            Array.Reverse(words);
            return string.Join(" ", words);
        }


        private static string RemovePunctuation(string name)
        {
           return  Regex.Replace(name, _regularExpressionWithPunctuationMarks, "");
        }
    }
}
