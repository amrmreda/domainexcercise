﻿using Domain.FuzzyStringLib.Models;

namespace Domain.FuzzyStringLib
{
    public interface IPropertyMatcher
    {
        bool IsMatch(Property agencyProperty, Property databaseProperty);
    }
}
