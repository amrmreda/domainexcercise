﻿using Domain.FuzzyStringLib.Helpers;
using Domain.FuzzyStringLib.Models;

namespace Domain.FuzzyStringLib.PropertyMatchers
{ 
    /// <summary>
    /// PropertyMatcher for "Only The Best Real Estate"
    /// </summary>
    public class OTBREPropertyMatcher : IPropertyMatcher
    {
        
        public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            if (agencyProperty != null && databaseProperty != null)
            {
             
                bool isNameMatching = StringHelper.CompareStrings(agencyProperty.Name, databaseProperty.Name, true);

                bool isAddressMatching = StringHelper.CompareStrings(agencyProperty.Address, databaseProperty.Address, true);
                
                return isNameMatching && isAddressMatching;  
            }
             
            return false;
        }



      
    }
}
