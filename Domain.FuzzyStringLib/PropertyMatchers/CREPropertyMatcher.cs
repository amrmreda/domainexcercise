﻿using Domain.FuzzyStringLib.Helpers;
using Domain.FuzzyStringLib.Models;

namespace Domain.FuzzyStringLib
{
    /// <summary>
    /// PropertyMatcher for "Contrary Real Estate"
    /// </summary>
    public class CREPropertyMatcher : IPropertyMatcher
    {
        public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            if (agencyProperty != null && databaseProperty != null)
            {
               var reversedName = StringHelper.ReverseWords(agencyProperty.Name);


                return StringHelper.CompareStrings(reversedName, databaseProperty.Name);
            }

            return false;
        }
    }
}
