﻿using Domain.FuzzyStringLib.Helpers;
using Domain.FuzzyStringLib.Models;

namespace Domain.FuzzyStringLib
{
    /// <summary>
    /// PropertyMatcher for "Location Real Estate"
    /// </summary>
    public class LREPropertyMatcher : IPropertyMatcher
    {
        public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            if (agencyProperty != null && databaseProperty != null)
            {
                bool isAgencyCodeMatching = StringHelper.CompareStrings(agencyProperty.AgencyCode, databaseProperty.AgencyCode);

                bool isLocationMatching = agencyProperty.Latitude == 1 || agencyProperty.Longitude == 111;

                return isAgencyCodeMatching && isLocationMatching;
            }

            return false;
        }
    }
}
