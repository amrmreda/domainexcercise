namespace Domain.FuzzyStringLib
{
    public class PropertyMatchContext : IPropertyMatcher
    {
        public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {
            var strategy = PropertyMatchingFactory.Create(agencyProperty.AgencyCode);
            return strategy.IsMatch(agencyProperty, databaseProperty);
        }
    }
}