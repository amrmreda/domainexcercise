﻿using Domain.FuzzyStringLib.Helpers;
using NUnit.Framework;

namespace Domain.FuzzyStringLib.Tests.HelpersTest
{
    /// <summary>
    ///This is a test class for ReverseWordsTest and is intended
    ///to contain all ReverseWordsTest Unit Tests
    ///</summary>
    [TestFixture]
    public class StringHelperTest
    {

        [Test]
        public void CompareStrings_CompareStrings_NoPunctaution_ReturnTrue()
        {
           string stringA = "Test me first";
           string stringB = "TEST me FIRST";

           Assert.IsTrue(StringHelper.CompareStrings(stringA, stringB));
        }

        [Test]
        public void CompareStrings_CompareStrings_NoPunctaution_ReturnFalse()
        {
            string stringA = "Test me first";
            string stringB = "Test me second";

            Assert.IsFalse(StringHelper.CompareStrings(stringA, stringB));
        }


        [Test]
        public void CompareStrings_CompareStrings_WithPunctaution_ReturnTrue()
        {
            string stringA = "Test me first";
            string stringB = "TEST me* FIRST!";

            Assert.IsTrue(StringHelper.CompareStrings(stringA, stringB, true));
        }

        [Test]
        public void CompareStrings_CompareStrings_WithPunctaution_ReturnFalse()
        {
            string stringA = "Test me first";
            string stringB = "T!st me** second";

            Assert.IsFalse(StringHelper.CompareStrings(stringA, stringB));
        }


        [Test]
        public void ReverseWords_EmprtString_DoNothing()
        {
            string text = string.Empty;
            string expected = string.Empty;
            string actual = StringHelper.ReverseWords(text);


            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ReverseWords_WhiteSpaceString_RetuenOriginalString()
        {
            string text = " ";
            string expected = " ";
            string actual = StringHelper.ReverseWords(text);


            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ReverseWords_StringWithTwoWhiteSpaces_ReturnReversedString()
        {
            string text = "Test the first";
            string expected = "first the Test";
            string actual = StringHelper.ReverseWords(text);


            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ReverseWords_StringWithWhiteSpaceAtTheEnd_ReturnReversedString()
        {
            string text = "Test the first ";
            string expected = " first the Test";
            string actual = StringHelper.ReverseWords(text);


            Assert.AreEqual(expected, actual);
        }
    }
}
