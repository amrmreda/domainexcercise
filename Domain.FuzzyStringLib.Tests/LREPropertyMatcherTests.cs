﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.FuzzyStringLib.Models;
using NUnit.Framework;

namespace Domain.FuzzyStringLib.Tests
{
    [TestFixture]
    public class LREPropertyMatcherTests
    {
        [Test]
        public void MatchingProperty_MatchingAgencyCode_ReturnTrue()
        {
            IPropertyMatcher matcher = new LREPropertyMatcher();

            var databaseProperty = new Property
            {
                AgencyCode = "LRE", 
                Longitude = 111,
                Latitude = 1
            };


            var agencyProperty = new Property
            {
                AgencyCode = "LRE",
                Longitude = 111,
                Latitude = 1
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void MatchingProperty_MatchingAgencyCode_ReturnFalse()
        {
            IPropertyMatcher matcher = new LREPropertyMatcher();

            var databaseProperty = new Property
            {
                AgencyCode = "OZyR3",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW.",
                Longitude = 111,
                Latitude = 1
            };


            var agencyProperty = new Property
            {
                AgencyCode = "LRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW.",
                Longitude = 111,
                Latitude = 1
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.False(result);
        }

        [Test]
        public void MatchingProperty_MatchingLocation_EqualOrLessThan200m_ReturnTrue()
        {
            IPropertyMatcher matcher = new LREPropertyMatcher();

            var databaseProperty = new Property
            {
                AgencyCode = "LRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW.",
                Longitude = 111,
                Latitude = 1
            };


            var agencyProperty = new Property
            {
                AgencyCode = "LRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW.",
                Longitude = 111,
                Latitude = 1
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void MatchingProperty_MatchingLocation_EqualOrLessThan200m_ReturnFalse()
        {
            IPropertyMatcher matcher = new LREPropertyMatcher();

            var databaseProperty = new Property
            {
                AgencyCode = "LRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW.",
                Longitude = 111,
                Latitude = 1
            };


            var agencyProperty = new Property
            {
                AgencyCode = "LRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW.",
                Longitude = 111,
                Latitude = 2
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }
    }
}
