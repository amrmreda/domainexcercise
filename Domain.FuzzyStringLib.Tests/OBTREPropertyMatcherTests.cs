﻿using Domain.FuzzyStringLib.Models;
using Domain.FuzzyStringLib.PropertyMatchers;
using NUnit.Framework;

namespace Domain.FuzzyStringLib.Tests
{
    [TestFixture]
    public class OBTREPropertyMatcherTests
    {
        [Test]
        public void MatchingPropertyNameWithPunctuation_ReturnTrue()
        {

            IPropertyMatcher matcher = new OTBREPropertyMatcher();

            var databaseProperty = new Property {
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };


            var agencyProperty = new Property
            { 
                Name = "*Super*-High! Apartments (Sydney)",
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void MatchingProperty_NameWithPunctuation_ReturnFalse()
        {

            IPropertyMatcher matcher = new OTBREPropertyMatcher();

            var databaseProperty = new Property
            { 
                Name = "Super High Apartments, Melbourne",
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };


            var agencyProperty = new Property
            { 
                Name = "Super High Apartments, Sydney", 
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsFalse(result);
        }
         
        [Test]
        public void MatchingPropertyAddressWithPunctuation_ReturnTrue()
        {

            IPropertyMatcher matcher = new OTBREPropertyMatcher();

            var databaseProperty = new Property
            {
                AgencyCode = "OTBRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };


            var agencyProperty = new Property
            {
                AgencyCode = "OTBRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 **Sir John-Young Crescent!, Sydney, NSW."
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }
         
        [Test]
        public void MatchingProperty_AddressWithPunctuation_ReturnFalse()
        {

            IPropertyMatcher matcher = new OTBREPropertyMatcher();

            var databaseProperty = new Property
            {
                AgencyCode = "OTBRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };


            var agencyProperty = new Property
            {
                AgencyCode = "OTBRE",
                Name = "Super High Apartments, Sydney",
                Address = "32 **Sir John-Young Crescent!, Sydney, NSW."
            };


            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }
         
    }
}
