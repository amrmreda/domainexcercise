﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.FuzzyStringLib.Models;
using NUnit.Framework;

namespace Domain.FuzzyStringLib.Tests
{
    [TestFixture]
    public class CREPropertyMatcherTests
    {
        [Test]
        public void IsMatch_AgencyPropertyNameReversed_ReturnTrue()
        {
            IPropertyMatcher matcher = new CREPropertyMatcher();

            var databaseProperty = new Property
            {
                Name = "The Summit Apartments"
            };


            var agencyProperty = new Property
            {
                Name = "Apartments Summit The"
            };

            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsTrue(result);
        }

        [Test]
        public void IsMatch_AgencyPropertyNameReversed_ReturnFalse()
        {
            IPropertyMatcher matcher = new CREPropertyMatcher();

            var databaseProperty = new Property
            {
                Name = "The Summit Apartments"
            };


            var agencyProperty = new Property
            {
                Name = "The Summit Apartments"
            };

            var result = matcher.IsMatch(agencyProperty, databaseProperty);

            //assert
            Assert.IsFalse(result);
        }
    }
}
